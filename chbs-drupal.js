var chbs = chbs || {};

(function($){
  $(document).ready(function() {
    // take the module settings and stuff them into the library settings
    chbs.id = Drupal.settings.chbs.id;
    chbs.length = Drupal.settings.chbs.length;
    chbs.separator = Drupal.settings.chbs.separator;
    chbs.camel_case = Drupal.settings.chbs.camel_case;
    chbs.add_number = Drupal.settings.chbs.add_number;
    // call the password gen function
    chbs_init();
  });
})(jQuery);
