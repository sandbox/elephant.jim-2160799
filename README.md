This is a module to provide password suggestions to users. The suggestions are easier to remember but as tough to crack as what is thought of as hard-to-crack passwords. The suggestions, however, are longer than -- have more characters than -- conventional hard-to-crack passwords. See http://xkcd.com/936/

==Installation and usage==

This module can be installed like any other.  The configuration page can be reached at /admin/config/chbs.

This module alters the forms on the user profile edit and registraton pages.  It inserts a prefix just before the password fields; the prefix is a placeholder for Javascript code to add text suggesting a password for users.  Note that the server never sees the suggestion, and the user is free to ignore the suggestion and use their own password choice.

The configuration page offers several options.  You can set the HTML element ID of the suggestion to make styling easier.  You can choose how many words to use in the suggested password; the default dictionary contains almost 3,000 words so each additional word adds more than 11 bits of entropy.  You can choose to use whatever character(s) you please to separate the words; this is sometimes necessary for password checkers that require a non-alphanumeric character in the password.  You can choose whether or not to camelcase the words, and whether or not to add a single digit at the end (for password checkers that require a non-alphabetic character in the password).  For debugging purposes, you can choose to not use the minified chbs.js library.
